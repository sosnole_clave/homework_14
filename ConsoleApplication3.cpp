﻿#include <iostream>
#include <cmath>

class Vector
{
public:
	Vector() : x(0), y(0), z(0)
	{}
	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
	{}
	void Show()
	{
		std::cout << x << ", " << y << ", " << z << "\n";
	}
	double length()
	{
		return sqrt(x * x + y * y + z * z);
	}

private:
	double x;
	double y;
	double z;
};

int main()
{
	Vector v(7, 6, 2);
	v.Show();
	std::cout << "Vector length: " << v.length();
}
